defmodule Trycsi do
  @service_name TrycsiService
  @module_name TrycsiService

  def start(), do: :csi.start(@service_name, @module_name)
  def start(), do: :csi.start_link(@service_name, @module_name)
  def stop(), do: :csi.stop(@service_name, @module_name)
  def foox(val), do: :csi.post_p(@service_name, :foo, [val])
  def fooy(val), do: :csi.call_p(@service_name, :foo, [val])
  def fooz(val), do: :csi.call_s(@service_name, :foo, [val])
  def loop(val), do: :csi.call_s(@service_name, :loop, [val])
  def time_and_a_word(), do: :csi.call_p(@service_name, :timed, [:good], 2000)
  def time_and_a_word_with_receive(), do: :csi.post_p(@service_name, :timed, [:good], 2000)
end
