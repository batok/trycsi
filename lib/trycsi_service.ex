defmodule TrycsiService do
  @behaviour :csi_server

  @state %{}

  @impl :csi_server
  def init(args, servicestate), do: boot(:init, args, servicestate) 
  
  @impl :csi_server
  def init_service(initargs), do: boot(:init_service, initargs)

  @impl :csi_server
  def terminate(reason, state), do: finish(:terminate, reason, state)
  
  @impl :csi_server
  def terminate_service(reason, state), do: finish(:terminate_service, reason, state)

  defp boot(source, x, y \\ nil) do
    IO.puts "#{source} args or initargs"
    IO.inspect x
    unless is_nil(y) do
      IO.puts "#{source} servicestate"
      IO.inspect y
    end
    {:ok, @state}
  end

  defp finish(source, reason, state) do
    IO.puts "#{source} #{inspect reason} #{inspect state}"
  end

  def foo(args, state) do
    [val] = args
    {val, state}
  end
 
  def loop([0], state) do
    {0, state}
  end

  def loop(args, state) do
    [val] = args
    IO.puts "val is #{val}"
    loop([val-1], state)
  end

  def timed(args, state) do
    :timer.sleep 3000
    {:ok, state}
  end

end
