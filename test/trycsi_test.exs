defmodule TrycsiTest do
  use ExUnit.Case
  doctest Trycsi

  setup do
    Trycsi.start()
    :ok
  end

  test "fooz" do
    assert Trycsi.fooz(:foo) == :foo
  end

  test "fooy" do
    assert Trycsi.fooy(:iam_fooy) == :iam_fooy
  end

  test "foox" do
    {:posted, _pid1, {_pid2, ref}} =
    Trycsi.foox(:iam_foox)
    receive do
      {ref, val} -> assert val == :iam_foox
      _ -> assert 0 == 1
    end
  end

  test "stats" do
    1..8 |> Enum.map(fn x -> Trycsi.fooz(x) end)
    [{{:response_time, :foo},
              {val, _, _, _, _}},
             {{:req_per_sec, :foo}, _}] = :csi.stats_get_all(TrycsiService)
    assert val == 11
  end

  test "loop" do
    assert Trycsi.loop(9) == 0
  end

  test "timeout" do
    # this test
    # that timeout as a parameter of the call was first than the duration of the function
    # so an error in a tuple is returned
    
    assert Trycsi.time_and_a_word() == {:error, :timeout_killed}
  end

  test "timeout using receive" do
    # this test
    # that timeout as a parameter of the call was first than the duration of the function
    # so an error in a tuple is returned as a message that can be caught by a receive
    
    {:posted, _pid1, {_pid2, ref}} =
    Trycsi.time_and_a_word_with_receive()
    receive do
      {ref, {:error, :timeout_killed}} -> assert 1 == 1
      _ -> 1 == 2
    end
  end


end
